const buttonEx1 = document.querySelector("#buttonEx1");

buttonEx1.addEventListener("click", function () {
  const resultEx1 = document.querySelector("#resultEx1");
  let count = 0;
  let n = 0;
  while (n < 10000) {
    count++;
    n += count;
  }
  resultEx1.innerHTML = `=> ${count}`;
});

const buttonEx2 = document.querySelector("#buttonEx2");

function tinhTong() {
  const resultEx2 = document.querySelector("#resultEx2");
  const xEl = document.querySelector("#so-x").value * 1;
  const nEl = document.querySelector("#so-n").value * 1;
  let luyThua = 1;
  let sum = 0;
  for (let i = 1; i <= nEl; i++) {
    // lũy thừa từng phần
    luyThua = Math.pow(xEl, i);

    sum += luyThua;
  }
  resultEx2.innerHTML = `=> ${sum}`;
}

buttonEx2.addEventListener("click", function () {
  tinhTong();
});

const buttonEx3 = document.querySelector("#buttonEx3");

function giaiThua() {
  const resultEx3 = document.querySelector("#resultEx3");
  const nEl = document.querySelector("#nhap-so-n").value * 1;
  let giaiThua = 1;
  for (let i = 1; i <= nEl; i++) {
    giaiThua *= i;
  }
  resultEx3.innerHTML = `=> ${giaiThua}`;
}

buttonEx3.addEventListener("click", function () {
  giaiThua();
});

const buttonEx4 = document.querySelector("#buttonEx4");

function taoTheDiv() {
  const content = document.querySelector("#content");
  let contentHTML = "";
  for (let i = 1; i <= 10; i++) {
    if(i % 2 == 0) {
        let chan = `<div class="p-2 mt-2 bg-danger text-white h6">Div chẵn ${i}</div>`
        contentHTML += chan
    }else {
        let le = `<div class="p-2 mt-2 bg-primary text-white h6">Div le ${i}</div>`
        contentHTML += le
    }
  }
  content.innerHTML = contentHTML
}

buttonEx4.addEventListener("click", function () {
  taoTheDiv();
});
